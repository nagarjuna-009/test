//
//  AppDelegate.h
//  Test
//
//  Created by nag nagarjuna on 23/06/17.
//  Copyright © 2017 nag nagarjuna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

