//
//  SelectCategoryVC.h
//  Mashref
//
//  Created by SystemRapid on 01/05/17.
//  Copyright © 2017 SystemRapid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectQualficationVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *selectQuelification_Tab;
@property(nonatomic, strong) NSMutableArray *selectedRows_arr;

@end
