//
//  SelectCategoryVC.m
//  Mashref
//
//  Created by SystemRapid on 01/05/17.
//  Copyright © 2017 SystemRapid. All rights reserved.
//

#import "SelectQualficationVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "QuelificationCell.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@interface SelectQualficationVC ()
{
    NSMutableDictionary *catData;
    NSMutableArray *phone_cat_en_arr,*phone_cat_ar_arr,*cat_ID_arr;
    NSString *catID;
    
}
@end

@implementation SelectQualficationVC
@synthesize selectedRows_arr;

AppDelegate *apdSelc_cata;
NSNumber *Selected_row;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    apdSelc_cata=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    catData =[[NSMutableDictionary alloc]init];
    phone_cat_ar_arr=[NSMutableArray new];
    phone_cat_en_arr=[NSMutableArray new];
    cat_ID_arr=[NSMutableArray new];
    selectedRows_arr=[NSMutableArray new];
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];

    [SVProgressHUD showWithStatus:@"Please wait"];

    [self GetMasterDataService];
    
    
    
   
}
-(void) GetMasterDataService
{
    NSMutableString *nbu1=[NSMutableString stringWithFormat:@"https://evaluation-exercise.herokuapp.com/getMasterData"];
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager GET:nbu1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"GetMasterData JSON: %@", [operation responseString]);
        NSLog(@"GetMasterData code = %li", (long)operation.response.statusCode);
        
        NSData *dataa = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSDictionary *jsonDic=[NSJSONSerialization JSONObjectWithData:dataa options:kNilOptions error:&error];
        NSLog(@"GetMasterData Responce %@", jsonDic);
        NSDictionary *masterData_dic=[jsonDic objectForKey:@"MasterData"];
        NSArray *json_arr=[masterData_dic valueForKey:@"EducationalQualification"];
        NSLog(@"json_arr.....%@",json_arr);
        [SVProgressHUD dismiss];
        
        //NSLog(@"rowsss......%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedRows_arr"]);
        
        [self.navigationItem.rightBarButtonItem setEnabled:YES];

        if(Sel_cate_done_clk)
        {
            for(int i = 0; i < [json_arr count]; i++)
            {
                NSDictionary *dc = [[NSDictionary alloc] initWithDictionary:[json_arr objectAtIndex:i]];
                [phone_cat_en_arr addObject:[dc objectForKey:@"Qualification"]];
                
            }
       
            selectedRows_arr=[[[NSUserDefaults standardUserDefaults] arrayForKey:@"selectedRows_arr"] mutableCopy];

        }
        else
        {
            for(int i = 0; i < [json_arr count]; i++)
            {
                NSDictionary *dc = [[NSDictionary alloc] initWithDictionary:[json_arr objectAtIndex:i]];
                [selectedRows_arr addObject:[NSNumber numberWithInt:0]];
                [phone_cat_en_arr addObject:[dc objectForKey:@"Qualification"]];
                
            }
           
        }
        
        [self.selectQuelification_Tab reloadData];
        
    }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     
     {
         self.navigationController.navigationBar.userInteractionEnabled = YES;
         NSLog(@"Error +++ %@", [error description]);
         NSLog(@"Error PROPER %@", [operation responseString]);
         NSLog(@"code = %i", operation.response.statusCode);
         
         if ([operation responseString] == nil || [[operation responseString] isKindOfClass:[NSNull class]]||operation.response.statusCode == 0)
         {
             
             UIAlertView *alertFail = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to get response, please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             
             [alertFail show];

             return;
         }
         NSData *dataa = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *json1;
         json1 = [[NSDictionary alloc]init];
         json1 = [NSJSONSerialization JSONObjectWithData:dataa options:kNilOptions error:&error];
         NSLog(@"json ************ VC Authorise ERROR - RESP %@", json1);
         NSLog(@"Authorise code = %li", (long)operation.response.statusCode);
         
     }];
    
}


- (IBAction)backAction:(id)sender {
    
    // PhoneBookViewController *PhoneBookViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneBookViewController"];
    
    // [self.navigationController pushViewController:PhoneBookViewController animated:YES];
        
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)DoneClk:(id)sender
{
    NSMutableArray *sel_cat_arr=[NSMutableArray new];
    NSMutableArray *sel_cat_id_arr=[NSMutableArray new];

    for(int i = 0; i < [selectedRows_arr count]; i++)
    {
        int temp = [[selectedRows_arr objectAtIndex:i] intValue];
        if (temp==1)
        {
            [sel_cat_arr addObject:[phone_cat_en_arr objectAtIndex:i]];
                //[sel_cat_id_arr addObject:[cat_ID_arr objectAtIndex:i]];
            
        }
    
    }
    [[NSUserDefaults standardUserDefaults] setObject:selectedRows_arr forKey:@"selectedRows_arr"];
    
    Sel_cate_done_clk=@"1";
    //Selected_Category_ID=[sel_cat_id_arr componentsJoinedByString:@","];
    Selected_category = [sel_cat_arr componentsJoinedByString:@","];
    NSLog(@"Selected_category......%@",Selected_category);
    if ([Selected_category length] == 0)
    {
            UIAlertView *alertFail = [[UIAlertView alloc] initWithTitle:@"Masharef" message:@"Please select atleast one category" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertFail show];
       
    }
    else
    {
       [self.navigationController popViewControllerAnimated:YES];
    }
    
   
}

#pragma mark - table delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return phone_cat_en_arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    QuelificationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(!cell)
    {
        cell = [[QuelificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
        cell.Name_lbl.text = [phone_cat_en_arr objectAtIndex:indexPath.row];

    NSLog(@"selected rows 111......%@",selectedRows_arr);

    NSNumber *temp = [selectedRows_arr objectAtIndex:indexPath.row];
    int sel = [temp intValue];
    NSLog(@"sel.......%i",sel);
    
    if(sel == 1)
        [cell.Call_btn setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    else
        [cell.Call_btn setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    
    cell.Call_btn.tag = indexPath.row;
    [cell.Call_btn addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.Call_btn1.tag = indexPath.row;
    [cell.Call_btn1 addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) checkButtonTapped:(id)sender
{
    UIButton *tappedButton = (UIButton*)sender;
    int index = (int)tappedButton.tag;
    NSLog(@"Row button: %d", index);
    NSLog(@"selectedRows_arr.....%@",selectedRows_arr);
    int temp = [[selectedRows_arr objectAtIndex:index] intValue];
    
    if(temp == 0)
    {
        [selectedRows_arr replaceObjectAtIndex:index withObject:[NSNumber numberWithInt:1]];
    }
    else
    {
        [selectedRows_arr replaceObjectAtIndex:index withObject:[NSNumber numberWithInt:0]];
    }
    
    NSLog(@"selectedRows_arr......%@",selectedRows_arr);
    [_selectQuelification_Tab reloadData];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
