//
//  ViewController.m
//  Test
//
//  Created by nag nagarjuna on 23/06/17.
//  Copyright © 2017 nag nagarjuna. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "SelectQualficationVC.h"
#import "Constants.h"
#import "AFNetworking.h"
#import <CoreLocation/CoreLocation.h>
#import "SVProgressHUD.h"

@interface ViewController ()<UITextFieldDelegate,CLLocationManagerDelegate>
{
    UIActionSheet *actionAlert;
    AppDelegate *apdl;
    NSString *Gender_Type,*photo_str,*lat_str,*long_str;
    UIImage *choosenImage;
    NSMutableData *receivedData;

}
@property(nonatomic,strong) IBOutlet UITextField *FirstName_Tf,*LastName_Tf,*PhoneNo_Tf,*Email_Tf,*JobType_Tf,*Address_Tf;
@property(nonatomic,strong) IBOutlet UIButton *Male_btn,*Female_btn,*EducationQlf_btn;
@property(nonatomic,strong) IBOutlet UILabel *gender_bg_lbl;
@property (nonatomic, strong) IBOutlet UIImageView *ProfileImageView;

@property (strong, nonatomic) IBOutlet UITableView *autoCompleteTableView;
@property (nonatomic, retain) NSMutableArray *autoCompleteArray;
@property (nonatomic, retain) NSArray *autoCompleteFilterArray;

@end
@implementation ViewController
@synthesize autoCompleteArray,autoCompleteTableView,autoCompleteFilterArray;

int picClick = -1;
CLLocationManager* locationManager ;
CLLocationCoordinate2D circleCenter;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
  
    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBar.hidden=NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    apdl=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    picClick = -1;
    Gender_Type=@"";
    receivedData=[NSMutableData data];

    self.gender_bg_lbl.layer.cornerRadius = 2;
    self.gender_bg_lbl.layer.borderWidth = 1.0;
    self.gender_bg_lbl.layer.borderColor = [[UIColor colorWithRed:42.0f/255.0f green:56.0f/255.0f blue:66.0f/255.0f alpha:1.0f]CGColor];
    
    self.EducationQlf_btn.layer.cornerRadius = 2;
    self.EducationQlf_btn.layer.borderWidth = 1.0;
    self.EducationQlf_btn.layer.borderColor = [[UIColor colorWithRed:42.0f/255.0f green:56.0f/255.0f blue:66.0f/255.0f alpha:1.0f]CGColor];

    actionAlert=[[UIActionSheet alloc] initWithTitle:@"Select" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery",@"Camera",nil];
    
    [self GetMasterDataService];
    [self Address];
}

-(void) viewWillAppear:(BOOL)animated
{
    if (Selected_category.length==0)
    {
        
        [self.EducationQlf_btn setTitle:@"EDUCATION QUELIFICATION" forState:UIControlStateNormal];
        
    }
    else
    {
        [self.EducationQlf_btn setTitle:Selected_category forState:UIControlStateNormal];
    }
}
- (IBAction)MaleClk:(id)sender
{
    Gender_Type=@"1";
    [_Male_btn setImage:[UIImage imageNamed:@"radio_on.png"] forState:UIControlStateNormal];
    [_Female_btn setImage:[UIImage imageNamed:@"radio_off.png"] forState:UIControlStateNormal];
    
}
- (IBAction)Female:(id)sender
{
    Gender_Type=@"0";
    [_Female_btn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
    [_Male_btn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
    
}
- (IBAction)selectEdu_QulfAction:(id)sender
{
    SelectQualficationVC *sel_vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCategoryID"];
    [self.navigationController pushViewController:sel_vc animated:YES];

}
- (IBAction)selectProfile_Action:(id)sender
{
    picClick = 1;
    
    [actionAlert showInView:apdl.window];
}
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
        if (buttonIndex==0)
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
            picker.delegate = self;
            picker.allowsEditing = NO;
            
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
        if (buttonIndex==1)
        {
            
            if (![UIImagePickerController isSourceTypeAvailable:
                  UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Device has no camera"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [myAlertView show];
                
            }
            else
            {
                
                UIImagePickerController* picker1 = [[UIImagePickerController alloc] init];
                picker1.delegate = self;
                picker1.allowsEditing = YES;
                picker1.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                [self presentViewController:picker1 animated:YES completion:NULL];
            }
        }
}

- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info
{
        choosenImage = info[UIImagePickerControllerOriginalImage];
        if(picClick == 1)
        {
            self.ProfileImageView.image=choosenImage;
        }
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    
}

-(void) GetMasterDataService
{
    NSMutableString *nbu1=[NSMutableString stringWithFormat:@"https://evaluation-exercise.herokuapp.com/getMasterData"];
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager GET:nbu1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"GetMasterData JSON: %@", [operation responseString]);
        //NSLog(@"GetMasterData code = %li", (long)operation.response.statusCode);
        
        NSData *dataa = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSDictionary *jsonDic=[NSJSONSerialization JSONObjectWithData:dataa options:kNilOptions error:&error];
        //NSLog(@"GetMasterData Responce %@", jsonDic);
        NSDictionary *masterData_dic=[jsonDic objectForKey:@"MasterData"];
        NSArray *json_arr=[masterData_dic valueForKey:@"JobTypes"];
        //NSLog(@"json_arr.....%@",json_arr);
        autoCompleteArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"autocomplete"]];
        
        autoCompleteTableView.hidden =TRUE;
        
        autoCompleteTableView.tableFooterView = [UIView new];
        
        [[autoCompleteTableView layer] setMasksToBounds:NO];
        [[autoCompleteTableView layer] setShadowColor:[UIColor blackColor].CGColor];
        [[autoCompleteTableView layer] setShadowOffset:CGSizeMake(0.0f, 5.0f)];
        [[autoCompleteTableView layer] setShadowOpacity:0.3f];


            for(int i = 0; i < [json_arr count]; i++)
            {
                NSDictionary *dc = [[NSDictionary alloc] initWithDictionary:[json_arr objectAtIndex:i]];
                [autoCompleteArray addObject:[dc objectForKey:@"JobType"]];
                
            }
        
        [[NSUserDefaults standardUserDefaults] setObject:autoCompleteArray forKey:@"autocomplete"];
        
    }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     
     {
         self.navigationController.navigationBar.userInteractionEnabled = YES;
         NSLog(@"Error +++ %@", [error description]);
         NSLog(@"Error PROPER %@", [operation responseString]);
         NSLog(@"code = %i", operation.response.statusCode);
         
         if ([operation responseString] == nil || [[operation responseString] isKindOfClass:[NSNull class]]||operation.response.statusCode == 0)
         {
             
             UIAlertView *alertFail = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to get response, please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             
             [alertFail show];
             
             return;
         }
         NSData *dataa = [[operation responseString] dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *json1;
         json1 = [[NSDictionary alloc]init];
         json1 = [NSJSONSerialization JSONObjectWithData:dataa options:kNilOptions error:&error];
         NSLog(@"json ************ VC Authorise ERROR - RESP %@", json1);
         NSLog(@"Authorise code = %li", (long)operation.response.statusCode);
         
     }];
    
}

#pragma mark - UITextField Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *passcode = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"SELF CONTAINS %@",passcode];
    autoCompleteFilterArray = [autoCompleteArray filteredArrayUsingPredicate:predicate];
    if ([autoCompleteFilterArray count]==0)
    {
        autoCompleteTableView.hidden = TRUE;
    }
    else
    {
        autoCompleteTableView.hidden = FALSE;
    }
    
    [autoCompleteTableView reloadData];
    return TRUE;
    
}
#pragma mark - UITableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [autoCompleteFilterArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:nil];
    if (Cell ==nil) {
        Cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    Cell.textLabel.text = [NSString stringWithFormat:@"%@",[autoCompleteFilterArray objectAtIndex:indexPath.row]];
    Cell.contentView.backgroundColor = [UIColor whiteColor];
    Cell.backgroundColor = [UIColor clearColor];
    
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 20)];/// change size as you need.
    separatorLineView.backgroundColor = [UIColor clearColor];// you can also put image here
    [Cell.contentView addSubview:separatorLineView];

    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
    _JobType_Tf.text =Cell.textLabel.text;
    autoCompleteTableView.hidden=YES;
    
}
-(void)Address
{
    locationManager = [[CLLocationManager alloc] init] ;
    
    locationManager.delegate = self;
    
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    [locationManager startUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    
    circleCenter = [location coordinate];
    
    
    lat_str = [NSString stringWithFormat:@"%f", circleCenter.latitude];
    long_str = [NSString stringWithFormat:@"%f", circleCenter.longitude];
    NSLog(@"*dLatitude : %@", lat_str);
    
    NSLog(@"*dLongitude : %@",long_str);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:circleCenter.latitude
                               
                                                        longitude:circleCenter.longitude];
    
    [geocoder reverseGeocodeLocation:newLocation
     
                   completionHandler:^(NSArray *placemarks, NSError *error) {

                       if (error) {
                           
                           NSLog(@"Geocode failed with error: %@", error);
                           
                           return;
                           
                       }

                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           NSLog(@" place mark%@ ", addressDictionary);
                           NSString *address = [addressDictionary
                                                
                                                objectForKey:@"City"];
                           
                           NSString *city = [addressDictionary
                                             
                                             objectForKey:@"Country"];
                           
                           NSString *state = [addressDictionary
                                              
                                              objectForKey:@"SubLocality"];
                           
                           NSString *zipLocStr = [addressDictionary objectForKey:@"FormattedAddressLines"];

                           NSString *adres = [NSString stringWithFormat:@"%@,%@,%@", state, city, address];
                           
                           _Address_Tf.text=adres;
                           
                           //NSLog(@" ZIP %@ ",  zipLocStr);
                           
                       }

                   }];
    
}

- (IBAction)Save_clk:(id)sender
{
    [SVProgressHUD showWithStatus:@"Please wait"];

    
    NSString *base64Img  = [UIImagePNGRepresentation(self.ProfileImageView.image) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    NSArray *eduQue_str = [Selected_category componentsSeparatedByString:@", "];
    NSString *name_str=[NSString stringWithFormat:@"%@ %@",_FirstName_Tf.text,_LastName_Tf.text];

    
  NSString *postString=[NSString stringWithFormat:@"name=%@&gender=%@&phoneNumber=%@&emailId=%@&jobType=%@&photoBitmap=%@&address=%@&latitude=%@&longitude=%@&educaitonalQualification=%@",name_str,Gender_Type,_PhoneNo_Tf.text,_Email_Tf.text,_JobType_Tf.text,base64Img,_Address_Tf.text,lat_str,long_str,eduQue_str];
   // [self DataUploadService];


NSData *postData=[postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
[request setURL:[NSURL URLWithString:@"https://evaluation-exercise.herokuapp.com/profile"]];
[request setHTTPMethod:@"POST"];
[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
[request setHTTPBody:postData];

NSURLConnection * conn=[[NSURLConnection alloc]initWithRequest:request delegate:self];
[conn start];

}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
    NSLog(@"response is....%@",response);
    [SVProgressHUD dismiss];

    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    NSLog(@"getting data");
    [SVProgressHUD dismiss];

}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSError *error;
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&error];
    //NSLog(@"%@",arr);
    if (error!=nil)
    {
        NSLog(@"JSON Parsing Error %@",[error localizedDescription]);
    }
    else
    {
        NSLog(@"%@",arr);
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
