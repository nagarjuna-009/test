//
//  main.m
//  Test
//
//  Created by nag nagarjuna on 23/06/17.
//  Copyright © 2017 nag nagarjuna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
